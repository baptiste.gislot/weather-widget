import { useFetch } from './useFetch';
import { createUrl } from '../utilities';

const useHourly = ({ apiKey, lat, lon, start, end }) => {
  const url = createUrl({
    url: 'https://api.climacell.co/v3/weather/forecast/hourly',
    query: {
      apiKey,
      lat,
      lon,
      unit_system: 'si',
      fields: 'precipitation,temp,feels_like,weather_code',
      start_time: start.toISOString(),
      end_time: end.toISOString()
    }
  });

  return useFetch({ url });
};

const useRealTime = ({ apiKey, lat, lon }) => {
  const url = createUrl({
    url: 'https://api.climacell.co/v3/weather/realtime',
    query: {
        apikey,
        lat,
        lon,
        unit_system: 'si',
        fields: 'precipitation,temp,feels_like,weather_code',
    }
  });

  return useFetch({ url });
};

export { useHourly, useRealTime };