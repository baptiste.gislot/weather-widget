import { useState, useEffect } from 'react';

const useFetch = ({ url }) => {
  const [reponse, setReponse] = useState({});
  const [loading, setloading] = useState(true);
  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    setloading(true);
    fetch(url)
      .then(res => {
        if (res.status !== 200) {
          throw new Error(res.statusText);
        }
        return res.json();
      })
      .then(res => {
        setReponse(res);
        setloading(false);
      })
      .catch(() => {
        setHasError(true);
        setloading(false);
      });
  }, [url]);

  return [reponse, loading, hasError];
};

export { useFetch };